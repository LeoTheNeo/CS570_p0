/*Leonardo Gomez
 *Carroll
 *CS570
 *Due Date: February 9, 2018 at 11pm
 *
 *skeleton form inout2.c
 *
 * Synopsis  -
 * The getword() function gets one word from the input stream.
 * It returns 0 when there is no word, and only end-of-file is encountered;
 * It returns -10 when there is no word, and only newline is encountered
 * Otherwise, it returns the number of characters in the word
 * EXCEPTION: return the *negative* of the length for metacharacters, e.g.,
 * "|" returns -1, "|&" returns -2, etc.
 *
 * INPUT: a pointer to the beginning of a character string
 * OUTPUT: -1 or the number of characters in the word
 * SIDE EFFECTS: bytes beginning at address w will be overwritten.
 *   Anyone using this routine should have w pointing to an
 *   available area at least STORAGE bytes long before calling getword().
 
 * Objective -
 * Illustrates the use of a lexical analyser
 *
 * Note: the program counts metacharacters twice, this is to account for special case when
 * when you have metacharacters followed by non-metacharacters
 */

#include "getword.h"
#include <stdio.h>

int getword(char *w){
    
    // variables storing the current letter and the word count
    int iochar;
    int wordlength = 0;
    int negativeword = 0;
    int specialcase = 0;
    int metaarray[4] = {'<','>','|','&'};
    //will tell if the previous character was a metacharacter
    //int meta = 0;
    //Reads from Stdin until it encountrs end of file
    while ((iochar = getchar()) != EOF){
        
        //stores the every letter in array
        w[wordlength] = iochar;
        
        if (iochar != ' ') {
            
            //verifies that the word is not bigger than the Buffer
            // if it is, then it will store the last letter and start from the beginning
            if (wordlength >= (STORAGE-1)) {
                w[wordlength] = '\0';
                ungetc(iochar, stdin);
                return wordlength;
            }
            
            // 92 is the ASCII decimal for  '\'
            // if it encounters '\' it will ignore all the metacharacters
            if (iochar == 92) {
                switch (specialcase) {
                    case 0:
                        w[wordlength] = '\0';
                        wordlength --;
                        specialcase++;
                        break;
                    default:
                        if (wordlength > 0) {
                            w[wordlength] = '\0';
                            wordlength --;
                            specialcase++;
                            break;
                        }
                        break;
                }
                //checks special case when # is the first later
                //but it does not get trigger when we encounter \#
            }else if ((iochar == '#') && (wordlength <= 0 ) && (specialcase == 0)){
                w[wordlength+1] = '\0';
                return -1;
            }else{
                // cheacks if there is any metacharacters
                for (int i =0; i < 4; i++) {
                    if( metaarray[i] == iochar){
                        negativeword --;
                    }
                }
                //special case when there is only one metacharacter in between
                //many non-metacharacters similar to [hello&hello] vs [hello&&hello]
                if ((wordlength > 0)&& (negativeword < 0)&&(negativeword > -2)&&(specialcase==0)) {
                    ungetc(iochar, stdin);
                    w[wordlength] = '\0';
                    if (negativeword < -1) {
                        return (wordlength*-1);
                    }
                    return wordlength;
                    
                }
                //this is for a special case when you encounter '\' + metacharacter
                //followed by a #
                if ((iochar == '#') && (wordlength <= 0 ) && (specialcase<=0)) {
                    negativeword --;
                }
                //the reason for this swich statement is to check for non-metacharactes
                //after a metacharacter, it will also check for special cases
                //for example
                if(negativeword < 0){
                    switch (iochar) {
                        case '<':
                            negativeword--;
                            break;
                        case '>':
                            negativeword--;
                            break;
                        case '|':
                            negativeword--;
                            break;
                        case '&':
                            negativeword--;
                            break;
                        case '#':
                            negativeword--;
                            break;
                        default:
                            if (specialcase > 0) {
                                break;
                            }
                            ungetc(iochar, stdin);
                            w[wordlength] = '\0';
                            return (wordlength * -1);
                    }
                    
                }
                //covers the case where we encounter a '\' followed by a metacharacter
                // then on the 3rd spot we have a non-metacharacter
                // in a case like \&hello it will return n=[6] [&hello]
                if ((specialcase == 1)&&(negativeword <= -3)){
                    ungetc(iochar, stdin);
                    w[wordlength] = '\0';
                    return wordlength;
                    
                }
                //makes sure to output -10 when encounter newline
                //if at the end of sentence it will put back the newline
                if (iochar == '\n') {
                    w[wordlength] = '\0';
                    if (wordlength > 0) {
                        ungetc(iochar, stdin);
                    }else{
                        return -10;
                    }
                    
                    return wordlength;
                }
            }
            
            wordlength ++;
        }
        //this accounts for spaces
        //there is a couple of exceptions in case it encounters special character
        // like '\' and if it encounters metacharacters
        if (iochar == ' ') {
            if (specialcase <= 0) {
                if (wordlength > 0) {
                    w[wordlength] = '\0';
                    if (negativeword < 0) {
                        return (wordlength*-1);
                    } else {
                        return wordlength;
                    }
                    
                }
            }
            wordlength++;
        }
    }
    w[wordlength] = '\0';
    if (negativeword < 0) {
        if(specialcase > 0){
            return wordlength;
        }
        return (wordlength * -1);
    } else {
        return wordlength;
    }
}
