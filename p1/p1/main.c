//
//  main.c
//  p1
//
//  Created by Leonardo Gomez on 2/4/18.
//  Copyright © 2018 LeoTheNeo. All rights reserved.
//

#include "getword.h"

int main()

{
    int c;
    char s[STORAGE];
    
    for(;;) {
        (void) printf("n=%d, s=[%s]\n", c = getword(s), s);
        if (c == 0) break;
    }
}
