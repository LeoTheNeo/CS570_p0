//
//  getword.c
//  CS570
//
//  Created by Leonardo Gomez on 1/24/18.
//  Copyright © 2018 LeoTheNeo. All rights reserved.
//
#include "getword.h"
#include <stdio.h>

int getword(char *w){
    int iochar;
    int wordlength = 0;
    //int check = 0;
    
    //reads form the command line until end of file
    while ((iochar = getchar()) != EOF){
        w[wordlength] = iochar;
      
        if (iochar != ' ') {
            
            // If the program encounters # at the begining of the sentence it will return -1
            if ((wordlength <= 0) && (iochar == '#')) {
                w[wordlength+1] = '\0';
                return -1;
            }
            //if it encounters a newline then it will return -10
            if (iochar == '\n') {
                w[wordlength] = '\0';
                //the program will 'use up' the newline when its imediately after a word
                // if the newline is right after a word it will name sure it replaces it with abother '\n'
                if (wordlength > 0) {
                    ungetc(iochar, stdin);
                }else{
                    return -10;
                }
                // returns the lenght of the word is there is no '\n' after the word
                return wordlength;
            }
            wordlength ++;
        }
        //make sure it clears the W array and returns space after the word is completed
        if (iochar == ' ') {
            if (wordlength > 0) {
                w[wordlength] = '\0';
                return wordlength;
            }
            //wordlength++;
        }
    }
    w[wordlength] = '\0';
    return wordlength;
}

