int getword(char *w);
* (Note: the preceding line is an ANSI C prototype statement for getword().
  It will work fine with edoras' C compilers.)
* The getword() function gets one "word" from the input stream.
* A "word" is a [maximal] string consisting of anything except space, newline,
  and EOF (EOF is actually a signal, not a character)
  (Note: unlike most 'word' definitions, tabs do NOT separate words; so
  'house	cat' is considered ONE [size-9] word, not two [it would be two
  words if there was a space between 'house' and 'cat', but a tab doesn't count.)
* There is one EXCEPTION to the above rule: A word that STARTS with '#' abruptly
  ends there, becoming a one-character word. -1 is returned in this weird case.
* INPUT: a pointer to the beginning of a character array
  (This address is supplied to you in p0.c, as a parameter to getword() .)
* OUTPUT: -10, -1, 0, or the number of characters in the word
  (If we have collected some characters, return the size of the word once you
  encounter a space, newline or EOF.  But if EOF is encountered while the
  word size is still zero, then return 0 [and leave an empty string in the
  character array].)  Similarly, if a newline is encountered while the
  word size is still zero, then return -10 [and leave an empty string in the
  character array].  EXCEPTION: return -1 if the word was '#'.
* SIDE EFFECTS: bytes beginning at address w will be overwritten.
  (Normally, we should guard against writing beyond the end of an array,
  but you do NOT have to worry about that safeguard in p0.)

getword() SKIPS leading spaces, so if getword() is called and there are no more
words on the line, then w points to an empty string.  All strings, including the
empty string, will be delimited by a zero-byte (eight 0-bits), as per the normal
C convention (this delimiter is not 'counted' when determining the length of
the string that getword will report as a return value)


corresponding outputs:

no-brainer one # 
(with a single space after each word), p0 should print:
n=10, s=[no-brainer]
n=3, s=[one]
n=-1, s=[#]
n=-10, s=[]
n=0, s=[]

  trickier# t#o  master
(with extra spaces at the beginning, and newline right after the
'r' in "master"), p0 should print:
n=9, s=[trickier#]
n=3, s=[t#o]
n=6, s=[master]
n=-10, s=[]
n=0, s=[]
In the above case, the newline gets 'used up' to discover that the
word "master" has ended, but then ungetc() has to be used to put
the newline BACK on the input stream, so that it can be seen AGAIN
in the next call to getword().  (Note that '#' is NOT treated specially
unless it is the FIRST character in the word.)

And finally, a ZERO-line file (zero lines, meaning NO newlines) containing
  #different words
(with EOF coming right after the 's') should cause p0 to print:
n=-1, s=[#]
n=9, s=[different]
n=5, s=[words]
n=0, s=[]