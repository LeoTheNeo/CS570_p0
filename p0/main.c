/*             inout2.c
 *
 * Synopsis  - Takes input from the keyboard and echoes that
 *             input back to the terminal.
 *
 * Objective - Illustrates the use of an assignment statement
 *             as part of the test expression in a while loop.
 */

#include "getword.h"

int main()

{
    int c;
    char s[STORAGE];
    
    for(;;) {
        (void) printf("n=%d, s=[%s]\n", c = getword(s), s);
        if (c == 0) break;
    }
}
